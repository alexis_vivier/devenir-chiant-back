var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var fs      = require('fs');
var path      = require('path');
var pemFile = path.resolve(__dirname, '../private.pem');

router.get('/register', (req, res)=>{
    res.render('../views/signup.ejs')
});

router.get("/home", (req,res)=>{
    res.render('../views/home.ejs')
})

router.get('/login',(req,res)=>{
    res.render('../views/login.ejs')
});

module.exports = router