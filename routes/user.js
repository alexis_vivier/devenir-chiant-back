var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var fs      = require('fs');
var path      = require('path');
var pemFile = path.resolve(__dirname, '../private.pem');
/**
 * DATA MODEL :
 *  id_user: 1,
    pseudo_user: 'test',
    email_user: 'test@test.fr',
    password_user: 'testpassword',
    photo_user: 'path/to/testphoto',
    is_ban_user: 0,
    is_signaled_user: 0,
    is_active_user: 0,
    description_user: 'testetest' 
    */
router.get('/', (req, res, next)=> {
    res.send('test')
  });

router.post('/post', (req, res)=>{
    // query database to insert user
    var query = "INSERT INTO `utilisateurs` (pseudo_user,email_user,password_user, description_user,is_ban_user,is_active_user,is_signaled_user,photo_user) VALUES (?,?,?,?,0,0,0,?) ";  
    console.log(req.body)
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hash) => {
            let user = {
                pseudo_user : req.body.pseudo,
                email_user :req.body.email,
                password_user : hash ,
                description_user : req.body.description,
                is_ban_user: 0,
                is_active_user:1,
                is_signaled_user:0,
                photo_user: "ceci est une image"
            }
            // execute query
            db.query(query,
                [
                    user.pseudo_user,
                    user.email_user,
                    user.password_user,
                    user.description_user,
                    user.photo_user
                ],
                (err, result)=>{
                if (err) {
                    res.redirect(401, '/' );
                    console.log(err)
                }else{
                    console.log(result);
                    res.redirect(200, '/login');
                }
            });
        });
    });
});

router.post('/login_check', (req,res) => {
    //on recup la data du form
    let credentials = {
        login : req.body.email,
        password : req.body.password
    }
    //on requete la bdd
    var query = "select email_user, password_user FROM utilisateurs where email_user = ? limit 1"
    db.query(query,
        [
            credentials.login,
        ],
        (err, result)=>{
        if (err) {
            res.redirect('/login');
            console.log(err)
        }else{
            console.log(result[0].password_user);
            
            //check les hashages 
            bcrypt.compare(req.body.password, result[0].password_user, (err, response)=> {
                // response == true
                if (err) throw err
                if(response){
                    jwt.sign({ foo: 'bar' }, fs.readFileSync(pemFile), { algorithm: 'RS256' }, (err, token) => {
                        console.log(token);
                        res.render('home', {token: token, user : result[0]});
                    });
                }
            });
        }
    });
})

router.get('/all', (req, res) => {
let query = "SELECT * FROM `utilisateurs`"; // query database to get all the players
    // execute query
    db.query(query, (err, result)=>{
        if (err) {
            res.status(400).send('User non trouvé')
            console.log(err)
            res.redirect('/');
        }
        console.log(result);
        res.status(200).send( result)
    });
});
router.get('/getBy', (req, res) => {
    let query = "SELECT * FROM `posts` where "+ req.body.search+ " = "+ req.body.value; // query database to get all the players
        // execute query
        db.query(query, (err, result)=>{
            if (err) {
                res.status(400).send('User non trouvé')
                console.log(err)
                res.redirect('/');
            }
            console.log(result);
            res.status(200).send( result)
        });
    });

  module.exports = router