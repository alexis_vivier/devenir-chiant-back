export default {
    BASE_URL : "http://127.0.0.1:5000",
    POST_URL : '/posts',
    USER_URL : '/users',
    CATEGORY_URL : '/category',
    COMMENT_URL : '/comments',
}