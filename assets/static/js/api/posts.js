import config from './config'
export default {
    //url --> http://localhost:5000/users/...
    POSTS_BASE_URL = '/posts',
    post(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.POSTS_URL}/post`,
            type: 'POST',
            data: payload,
            success: function(data) {
                console.log(`User posted : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    put(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.POSTS_URL}/put`,
            type: 'PUT',
            data: payload,
            success: function(data) {
                console.log(`User updated : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    getAll(){
        $.ajax({
            url: `${config.BASE_URL}${config.POSTS_URL}/all`,
            type: 'GET',
            success: function(data) {
                console.log(`All posts : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    getBy(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.POSTS_URL}/getBy`,
            type: 'PUT',
            data: payload,
            success: function(data) {
                console.log(`Posts got by : ${payload.search}, returned : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    delete(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.POSTS_URL}}/delete`,
            type: 'PUT',
            data: payload,
            success: function(data) {
                console.log(`Post deleted`);
            },
            error: function(err){
                console.log(err)
            }
        });
    }
}