import config from './config'
export default {
    //url --> http://localhost:5000/users/...
    
    post(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.COMMENT_URL}/post`,
            type: 'POST',
            data: payload,
            success: function(data) {
                console.log(`Comment posted : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    put(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.COMMENT_URL}/put`,
            type: 'PUT',
            data: payload,
            success: function(data) {
                console.log(`Comment updated : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    getAll(){
        $.ajax({
            url: `${config.BASE_URL}${config.COMMENT_URL}/all`,
            type: 'GET',
            success: function(data) {
                console.log(`All comments : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    getBy(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.COMMENT_URL}/getBy`,
            type: 'PUT',
            data: payload,
            success: function(data) {
                console.log(`Comment got by : ${payload.search}, returned : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    delete(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.COMMENT_URL}}/delete`,
            type: 'PUT',
            data: payload,
            success: function(data) {
                console.log(`Comment deleted`);
            },
            error: function(err){
                console.log(err)
            }
        });
    }
}