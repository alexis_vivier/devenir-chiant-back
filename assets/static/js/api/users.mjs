import config from './config.mjs'
export default {
    //url --> http://localhost:5000/users/...
    
    post(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.USER_URL}/post`,
            type: 'POST',
            data: payload,
            success: function(data) {
                console.log(`User posted : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    put(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.USER_URL}/put`,
            type: 'PUT',
            data: payload,
            success: function(data) {
                console.log(`User updated : ${data}`);
               
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    getAll(){
        console.log(config.BASE_URL)
        $.ajax({
            url: `${config.BASE_URL}${config.USER_URL}/all`,
            type: 'GET',
            success: function(data) {
                console.log(`All users : ${data}`);
                return data
            },
            error: function(err){
                console.log('echec')
            }
        });
    },
    getBy(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.USER_URL}/getBy`,
            type: 'GET',
            data: payload,
            success: function(data) {
                console.log(`User got by : ${payload.search}, returned : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    delete(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.USER_URL}}/delete`,
            type: 'DELETE',
            data: payload,
            success: function(data) {
                console.log(`User deleted`);
            },
            error: function(err){
                console.log(err)
            }
        });
    }
}