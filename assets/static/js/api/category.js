import config from './config'
export default {
    //url --> http://localhost:5000/users/...
    /*payload = {
        slug : 'test slug',
        label : ' test'
    }*/
    post(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.CATEGORY_URL}/post`,
            type: 'POST',
            data: payload,
            success: function(data) {
                console.log(`Category posted : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    put(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.CATEGORY_URL}/put`,
            type: 'PUT',
            data: payload,
            success: function(data) {
                console.log(`Category updated : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    getAll(){
        $.ajax({
            url: `${config.BASE_URL}${config.CATEGORY_URL}/all`,
            type: 'GET',
            success: function(data) {
                console.log(`All categories : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    getBy(payload){
        //paload.search = id
        //payload.value = 1
        $.ajax({
            url: `${config.BASE_URL}${config.CATEGORY_URL}/getBy`,
            type: 'GET',
            data: payload,
            success: function(data) {
                console.log(`Category got by : ${payload.search}, returned : ${data}`);
            },
            error: function(err){
                console.log(err)
            }
        });
    },
    delete(payload){
        $.ajax({
            url: `${config.BASE_URL}${config.CATEGORY_URL}}/delete`,
            type: 'DELETE',
            data: payload,
            success: function(data) {
                console.log(`Category deleted`);
            },
            error: function(err){
                console.log(err)
            }
        });
    }
}